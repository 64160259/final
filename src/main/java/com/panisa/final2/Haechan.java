package com.panisa.final2;

public class Haechan extends Nct implements Singable {
    public Haechan(String unit, String national) {
        super(unit, national);
    }

    @Override
    public String toString() {
        return "Haechan("+this.getUnit()+")" + " national: "+ this.getNational();
    }

    @Override
    public void sing() {
        System.out.println(this.toString() + " Vocal.");
    }
}
