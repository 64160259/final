package com.panisa.final2;

public class Johnny extends Nct implements Rapable {
    public Johnny(String unit, String national) {
        super(unit, national);
    }

    @Override
    public String toString() {
        return "Johnny("+this.getUnit()+")" + " national: "+ this.getNational();
    }

    @Override
    public void rap() {
        System.out.println(this.toString() + " Rapper."); 
    }
    
}
