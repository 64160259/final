package com.panisa.final2;

public class Kun extends Nct implements Singable {
    public Kun(String unit, String national) {
        super(unit, national);
    }

    @Override
    public String toString() {
        return "Kun("+this.getUnit()+")" + " national: "+ this.getNational();
    }

    @Override
    public void sing() {
        System.out.println(this.toString() + " Vocal.");
    }
    
}
