package com.panisa.final2;

public class Mark extends Nct implements Rapable{
    public Mark(String unit, String national) {
        super(unit, national);
    }

    @Override
    public String toString() {
        return "Mark("+this.getUnit()+")" + " national: "+ this.getNational();
    }

    @Override
    public void rap() {
        System.out.println(this.toString() + " Rapper."); 
    }
}
