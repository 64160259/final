package com.panisa.final2;

public class Renjun extends Nct implements Singable {
    public Renjun(String unit, String national) {
        super(unit, national);
    }

    @Override
    public String toString() {
        return "Renjun("+this.getUnit()+")" + " national: "+ this.getNational();
    }

    @Override
    public void sing() {
        System.out.println(this.toString() + " Vocal.");
    }
}
