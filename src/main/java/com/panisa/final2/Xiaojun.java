package com.panisa.final2;

public class Xiaojun extends Nct implements Singable {
    public Xiaojun(String unit, String national) {
        super(unit, national);
    }

    @Override
    public String toString() {
        return "Xiaojun("+this.getUnit()+")" + " national: "+ this.getNational();
    }

    @Override
    public void sing() {
        System.out.println(this.toString() + " Vocal.");
    }
}
